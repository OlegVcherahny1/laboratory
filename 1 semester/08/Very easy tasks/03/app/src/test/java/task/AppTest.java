package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void sum() {
    assertEquals(15, App.sum(5));
  }
}
