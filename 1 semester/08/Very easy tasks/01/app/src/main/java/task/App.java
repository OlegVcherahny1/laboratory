package task;

// number - последний элемент последовательности (до какого надо вычеслять)
// answer - ответ

public class App {
  public static long[] fibonacci(int number) {
    long[] fibMass = new long[number];
    fibMass[0] = 0;
    fibMass[1] = 1;
    for (int i = 2 ;i < fibMass.length;i++ ) {
      fibMass[i] = fibMass[i - 1] + fibMass[i - 2];
    }
    return fibMass;
  }



  public static long fibRec(int number1, long[] massFib) {
    if (number1 <= 2) {
      massFib[number1 - 1] = number1 - 1;
      return massFib[number1 - 1];
    }
    else {
      long answer = fibRec(number1 - 1, massFib) + fibRec(number1 - 2, massFib);
      massFib[number1 - 1] = answer;
      return answer;
    }
  }



  public static long[] getFibRec(int number1) {
    if (number1 >= 1) {
      long[] massFib = new long[number1];
      fibRec(number1, massFib);
      return massFib;
    }
    else {
      return null;
    }
  }
}
