package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void testFibonacci() {
    long[] x = {0,1,1,2,3,5,8};
    assertArrayEquals(x,App.fibonacci(7));
  }
  @Test void testFibRec() {
    long[] x = {0,1,1,2,3,5,8};
    assertArrayEquals(x,App.getFibRec(7));
  }
}
