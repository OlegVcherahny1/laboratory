package task;

public class App {
  public static long factorial(int a) {
    long x = 1;
    for (int i = 1; i <= a; i++) {
      x = x * i;
    }
    return x;
  }
  public static long factorialRecursion(int a){
    if (a == 1) {
      return a;
    }
    else {
      long x = a * factorialRecursion(a - 1);
      return x;
    }
  }
}
