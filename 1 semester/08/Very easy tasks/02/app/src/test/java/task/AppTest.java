package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void test02Factorial() {
    assertEquals(120, App.factorial(5));
  }
  @Test void test02FactorialRecursion() {
    assertEquals(120, App.factorialRecursion(5));
  }
}
