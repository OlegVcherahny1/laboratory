public class Matrix {
  public static void main(String[] args) {
    //  1 2 3
    //  4 5 0
    //  2 3 4
    int[][] mass = new int[][]{{1, 2, 3},
                               {4, 5, 0},
                               {2, 3, 4} };
    for (int i = 0; i < mass.length; i++) {
      for (int j = 0; j < mass.length; j++) {
        System.out.print(mass[i][j]+ " ");
      }
    }
    System.out.println();
  }
}
