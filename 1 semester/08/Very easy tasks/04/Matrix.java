class Matrix {
  public static void main(String[] args) {
    // 1 2 3
    // 4 5 0
    // 2 3 4
    int[][] mass = new int[][]{{1, 2, 3, 6},
                               {4, 5, 0, 5},
                               {5, 3, 7, 1}};
    for (int i = 0; i < mass[0].length; i++) {
      for (int j = 0; j < mass.length; j++) {
        System.out.print(mass[j][i] + " ");
      }
      System.out.println();
    }
  }
}
