package task;

public class App {
  public static int symbolInStr(String stroka, char symbol){
    int counter = 0;
    for(int i = 0; i < stroka.length();i++){
      if(stroka.charAt(i) == symbol){
        counter++;
      }
    }
    return counter;
  }
}
