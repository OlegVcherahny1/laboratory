package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void matrixGenerationTest() {
    int quantity = 5;
    int strings = 5;
    int columns = 5;
    assertNotNull(App.matrixGeneration(quantity, strings, columns));
  }

  @Test void matrixErroneousGenerationTest() {
    int quantity = 0;
    int strings = 5;
    int columns = -1;
    assertNull(App.matrixGeneration(quantity, strings, columns));
  }

  @Test void matrixMultiplyTest() {
    int[][] matrix1 = {{4, 5}, {1, 7}, {8, 6}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}};
    int[][] finalMatrix = {{38, 27, 55}, {44, 24, 54}, {52, 42, 82}};
    assertArrayEquals(finalMatrix, App.multiplyMatrix(matrix1, matrix2));
  }

  @Test void erroneousMatrixMultiplyTest() {
    int[][] matrix1 = {{4, 5, 2}, {1, 7, 8}};
    int[][] matrix2 = {{2, 3, 5}, {6, 3, 7}, {1, 5, 0}, {8, 6, 9}};
    assertNull(App.multiplyMatrix(matrix1, matrix2));
  }
  
  @Test void matrixErroneous2MultiplyTest() {
    int[][] matrix1 = {{},{}};
    int[][] matrix2 = {{},{}};
    assertNull(App.multiplyMatrix(matrix1, matrix2));
  }
}
