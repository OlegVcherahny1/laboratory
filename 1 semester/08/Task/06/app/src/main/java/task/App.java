package task;

public class App {
  public static int[][][] matrixGeneration(int quantity, int strings, int columns) {
    if (quantity > 0 && strings > 0 && columns > 0) {
      int[][][] matrix = new int[quantity][strings][columns];
      for (int i = 0; i < quantity; i++) {
        for (int j = 0; j < strings; j++) {
          for (int q = 0; q < columns; q++) {
            int number = (int)(Math.random() * 100);
            matrix[i][j][q] = number;
          }
        }
      }
      return matrix;
    }
    else {
      return null;
    }
  }

  public static int[][] multiplyMatrix(int[][] matrix1, int[][] matrix2 ) {
    if ((matrix1.length != 0 && matrix2.length != 0) && (matrix1[0].length != 0 && matrix2[0].length != 0) && (matrix1[0].length == matrix2.length)) {
      int[][] answer = new int[matrix1.length][matrix2[0].length];
      for (int i = 0; i < matrix1.length; i++) {
        for (int j = 0; j < matrix2[0].length; j++) {
          for (int q = 0; q < matrix1[0].length; q++) {
            answer[i][j] += matrix1[i][q] * matrix2[q][j];
          }
        }
      }
      return answer;
    }
    else {
      return null;
    }
  }
}
