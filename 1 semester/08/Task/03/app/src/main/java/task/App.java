package task;

import java.util.Arrays;

public class App {
  public static void sort(double[] array) {
    double replacement = 0;
    int i = 0;
    while (i < array.length-1) {
      if (array[i] > array[i+1]) {
        replacement = array[i];
        array[i] = array[i+1];
        array[i+1] = replacement;
        i = 0;
      }
      else {
        i++;
      }
    }
  }
  public static double[] createSortedArray(double[] array1) {
    double[] array2 = array1.clone();
    sort(array2);
    return array2;
  }
}
