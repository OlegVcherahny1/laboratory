package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void createSortingArray() {
    double[] array1 = {1, 9, 2, 8, 3, 7, 4, 5, 6};
    double[] result = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    assertArrayEquals(result,App.createSortedArray(array1));
  }
  @Test void sorting() {
    double[] array1 = {1, 9, 2, 8, 3, 7, 4, 5, 6};
    assertTrue(array1 != App.createSortedArray(array1));
  }
  @Test void createSortingSingleArray() {
    double[] array1 = {1};
    double[] result = {1};
    assertArrayEquals(result,App.createSortedArray(array1));
  }
  @Test void sortingSingleArray() {
    double[] array1 = {1};
    assertTrue(array1 != App.createSortedArray(array1));
  }
  @Test void createSortingEmptyArray() {
    double[] array1 = {};
    double[] result = {};
    assertArrayEquals(result,App.createSortedArray(array1));
  }
  @Test void sortingEmptyArray() {
    double[] array1 = {};
    assertTrue(array1 != App.createSortedArray(array1));
  }
}
