import java.util.*;
import java.util.Random;

class RandomCoeff {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите порог: ");
    double numberThreshold = sc.nextDouble();
    System.out.println("Введите коэффициент влияния порога: ");
    double coeff = sc.nextDouble();
    System.out.println("Введите максимальное время моделирования: ");
    int maxTime = sc.nextInt();

    long start = System.currentTimeMillis();
    double maxNumber = coeff * numberThreshold;
    double minNumber = -maxNumber;
    int size = 5;
    int i = 0;
    int j = 0;
    double[][] mass = new double[size][size];
    Random randomTime = new Random();

    while (i < mass[0].length) {
      if (randomTime.nextBoolean() && i < mass[0].length) {
        double number = (Math.random() * (maxNumber - minNumber)) + minNumber;
        if (number < numberThreshold) {
          mass[0][i] = System.currentTimeMillis() - start;
          mass[1][i] = number;
          i++;
        }
      }
    }
    if (maxTime > System.currentTimeMillis() - start) {
      while (j < mass[0].length) {
        if (mass[1][j] != 0) {
          System.out.println("<" + mass[0][j] + ">: " + "<" + mass[1][j] + ">");
        }
        j++;
      }
    }
    else {
      System.out.println("\nПревышено время генерации чисел. ");
    }
  }
}
