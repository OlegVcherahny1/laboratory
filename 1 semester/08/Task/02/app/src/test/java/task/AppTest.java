package task;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void workingTime() {
    App generate = new App(100,5000);
    assertEquals(100, generate.getMainMass().length);
  }
  @Test void workingTimeErr() {
    App generate = new App(100000, 5);
    assertNull(generate.getMainMass());
  }
}
