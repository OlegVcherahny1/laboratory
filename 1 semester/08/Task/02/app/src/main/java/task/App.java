package task;

import java.util.*;

public class App {
  private int[] mass;
  private long time;
  private int threshold;

  App(int numbers, int threshold) {
    this.mass = new int[numbers];
    this.threshold = threshold;
    this.generate();
  }

  public void generate() {
    long start = System.currentTimeMillis();
    for(int i = 0; i < this.mass.length; i++) {
      this.mass[i] = (int)(Math.random() * 100);
    }
    this.time = System.currentTimeMillis() - start;
  }

  public int[] getMainMass() {
    if (this.time < this.threshold) {
      return this.mass;
    }
    else {
      return null;
    }
  }
}
