import java.util.Scanner;

class ProcessModulation {
  public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    System.out.println("Введите количество повторений");
    int repeat = sc.nextInt();
    System.out.println("Введите размер области");
    int size = sc.nextInt();
    System.out.println("Введите сдвиг относительно начала координат");
    int shift = sc.nextInt();
    int temp1 = repeat * 2;
    int temp2 = 2;
    int[][] mass = new int[temp1][temp2];
    int counter = 0;

    for (int i = 0; i < repeat; i++) {
      for (int j = 0; j < temp2; j++) {
        mass[i][j] = (int) (Math.random() * size) + shift;
      }
    }

    for (int i = 0;i < repeat ;i++ ) {
      for (int j = i + 1;j < repeat ;j++ ) {
        if ((mass[j][0] > mass[i][0] && mass[j][0] < mass[i][1]) || (mass[j][1] > mass[i][0] && mass[j][1] < mass[i][1])) {
          counter++;
        }
        else if ((mass[j][0] < mass[i][0] && mass[j][0] > mass[i][1]) || (mass[j][1] < mass[i][0] && mass[j][1] > mass[i][1])) {
           counter++;
        }
        else if (mass[j][0] < mass[i][0] && mass[j][1] > mass[i][1]) {
           counter++;
        }
      }
    }
    System.out.println(counter);
  }
}
