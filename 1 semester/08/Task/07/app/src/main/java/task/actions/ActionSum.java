package task.actions;

import java.util.*;

public class ActionSum extends Action {
  private String title;

  public ActionSum(String title) {
    super(title);
  }

  public void doSmth() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите первое число: ");
    int a = sc.nextInt();
    System.out.println("Введите второе число: ");
    int b = sc.nextInt();
    int sum = a + b;
    System.out.println("Сумма чисел равна: " + sum);
  }
}
