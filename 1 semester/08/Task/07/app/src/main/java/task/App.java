package task;

import task.actions.*;
import java.util.*;
import java.lang.*;

public class App {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int userInput = 0;
    int sizeChildren;
    Node currentNode = initNodes();

    while(currentNode != null) {
      sizeChildren = currentNode.sizeChildren();
      currentNode.print();
      userInput = sc.nextInt();
      System.out.println();
      if (userInput == 0) {
        currentNode = currentNode.getParent();
      }
      else if (userInput > 0 && userInput <= sizeChildren) {
        currentNode = currentNode.children.get(userInput - 1);
      }
      else if (userInput > sizeChildren && userInput <= sizeChildren + currentNode.sizeAction()) {
        currentNode.actions.get(userInput - sizeChildren - 1).doSmth();
      }
    }
  }

  public static Node initNodes() {
    Node start = new Node("");
    Node level = new Node("Начать работу");
    Node level1 = new Node("Простые операции");
    Node level2 = new Node("Сложные операции");
    Action sum = new ActionSum("Сложение");
    Action multipli = new ActionMultipli("Умножение");
    Action subtraction = new ActionSubtraction("Вычетание");
    Action fibonacci = new ActionFibonacci("Фибоначчи");
    Action factorial = new ActionFactorial("Факториал");
    Action division = new ActionDivision("Деление");
    start.setChildren(level);
    level.setChildren(level1);
    level.setChildren(level2);
    level1.setAction(sum);
    level1.setAction(multipli);
    level1.setAction(subtraction);
    level1.setAction(division);
    level2.setAction(fibonacci);
    level2.setAction(factorial);
    return start;
  }
}
