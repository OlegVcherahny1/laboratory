package task.actions;

import java.util.*;

public class ActionSubtraction extends Action {
  String title;

  public ActionSubtraction(String title) {
    super(title);
  }

  public void doSmth() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите первое число: ");
    int a = sc.nextInt();
    System.out.println("Введите второе число: ");
    int b = sc.nextInt();
    int subtraction = a - b;
    System.out.println("Разность чисел равна: " + subtraction);
  }
}
