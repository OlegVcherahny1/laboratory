package task.actions;

import java.util.*;

public class ActionMultipli extends Action {
  private String title;

  public ActionMultipli(String title) {
    super(title);
  }

  public void doSmth() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите первое число: ");
    int a = sc.nextInt();
    System.out.println("Введите второе число: ");
    int b = sc.nextInt();
    int multipli = a * b;
    System.out.println("Произведение чисел равна: " + multipli);
  }
}
