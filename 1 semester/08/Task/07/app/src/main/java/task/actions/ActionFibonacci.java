package task.actions;

import java.util.*;

public class ActionFibonacci extends Action {
  private String title;

  public ActionFibonacci(String title) {
    super(title);
  }

  public void doSmth() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите номер элемента последовательности: ");
    int number = sc.nextInt();
    long[] fibMass = new long[number];
    fibMass[0] = 0;
    fibMass[1] = 1;
    for (int i = 2 ;i < fibMass.length;i++ ) {
      fibMass[i] = fibMass[i - 1] + fibMass[i - 2];
    }
    System.out.println("Элемент под номером " + number + " равен: " + fibMass[fibMass.length-1]);
  }
}
