package task.actions;

import java.util.*;

public class ActionFactorial extends Action {
  private String title;

  public ActionFactorial(String title) {
    super(title);
  }

  public void doSmth() {
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите факториал какого числа нужно вычислить: ");
    long index = sc.nextInt();
    long number = 1;
    for (int i = 1; i <= index; i++) {
      number = number * i;
    }
    System.out.println("Факториал " + index + " равен " + number);
  }
}
