package task.actions;

public abstract class Action {
  private String title;
  public Action(String title) {
    this.title = title;
  }
  public String getTitle() {
    return this.title;
  }
  abstract public void doSmth();
  }
