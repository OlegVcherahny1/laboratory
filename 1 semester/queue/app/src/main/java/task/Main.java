package task;

public class Main {
  public static void main(String[] args) {
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(1);
    arrayQueue.add(2);
    arrayQueue.add(1);
    arrayQueue.add(2);
    System.out.println("Contains 2: " + arrayQueue.contains(2));
    System.out.println("Contains 3: " + arrayQueue.contains(3));
    System.out.println(arrayQueue.remove() + " " + arrayQueue.size());
    System.out.println(arrayQueue.remove() + " " + arrayQueue.size());
    System.out.println(arrayQueue.remove() + " " + arrayQueue.size());
    System.out.println(arrayQueue.remove() + " " + arrayQueue.size());
  }
}
