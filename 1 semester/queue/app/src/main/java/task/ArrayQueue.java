package task;

import java.util.*;

public class ArrayQueue<E> implements Queue<E> {
  private int pointer; // Индекс позиции на которую можно поставить элемент
  private int capacity; // Размер
  private E[] queue; // Массив с объектами

  public ArrayQueue() {
    this.pointer = -1;
    this.capacity = 2;
    this.queue = (E[]) new Object[capacity];
  }

  private ArrayQueue(E[] queue) { // Конструктер
    this.queue = queue;
  }

  @Override
  public int size() { // Возращает размер очереди
    return pointer + 1; // +1 так как у первого элемента индекс 0
  }

  @Override
  public boolean isEmpty() { // Проверяет не является ли очередь пустой // Возвращает значение true, если эта коллекция не содержит элементов.
    return pointer == -1;
  }

  @Override
  public boolean contains(Object e) { // Проверяет наличие объектов // Тоже самое что и isEmpty только с объектами
    for (int i = 0; i <= pointer; i++) { // Возвращает значение true, если эта коллекция содержит указанный элемент. Более формально, возвращает значение true тогда и только тогда, когда эта коллекция содержит хотя бы один элемент e, такой, что (o==null ? e==null : o.равно(e)).
      if (queue[i] == e) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator<E> iterator() { //Итератор возвращает итератор
    return new Iterator<E>() {
      final ArrayQueue<E> q = new ArrayQueue<>(queue);
      @Override
      public boolean hasNext() {
        return !q.isEmpty();
      }

      @Override
      public E next() {
        return q.remove();
      }
    };
  }

  @Override
  public Object[] toArray() { // Преобразует очередь в массив
    return Arrays.copyOf(queue, pointer + 1);
  }

  @Override
  public Object[] toArray(Object[] a) { // Тоже самое что и toArray только с объектами
    return Arrays.copyOf(queue, pointer + 1);
  }

  @Override
  public boolean add(E e) { // Добавляет в очередь объект
    if ((pointer + 1) == capacity) {
      expand(capacity * 2);
    }
    pointer++;
    queue[pointer] = e;
    return true;
  }

  private void expand(int newCapacity) { // Увеличить объем выделенной памяти без потери данных
    capacity = newCapacity;
    queue = Arrays.copyOf(queue, newCapacity);
  }

  @Override
  public boolean remove(Object e) { // Метод удаления последнего элемента
    for (int i = 0; i < size(); i++) {
      if( queue[i] == e) {
        for (int j = i; j < size() - 1; j++) {
          queue[j] = queue[j + 1];
        }
        pointer--;
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends E> collection) { // Добавляет колекцию С
    for (E element : collection) {
      add(element);
    }
    return true;
  }

  @Override
  public void clear() { // Очистка очереди
    pointer = -1;
  }

  @Override
  public boolean retainAll(Collection c) { // Удаляет все элементы которых нет в колекции С
    for (Object element : queue) {
      if (!c.contains(element)) {
        remove(c);
      }
    }
    return true;
  }

  @Override
  public boolean removeAll(Collection c) { // Удаляет все элементы которые есть колекции С
    for (Object element : c) {
      if (contains(element)) {
        remove(element);
      }
    }
    return true;
  }

  @Override
  public boolean containsAll(Collection c) { // Проверяет все ли элементы есть в колекции С
    for (Object element : c) {
      if (!contains(element)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean offer(E e) { // Делает тоже самое что и add
    return add(e);
  }

  @Override
  public E remove() { // Удалить и вернуть верхний элемент
    if (!isEmpty()) {
      E e = queue[pointer]; // Получаем объект
      pointer--;
      return e;
    }
    else {
      throw new NoSuchElementException();
    }
  }

  @Override
  public E poll() { // Тоже самое что и remove только возращает null
    if (!isEmpty()) {
      E e = queue[pointer];
      pointer--;
      return e;
    }
    else {
      return null;
    }
  }

  @Override
  public E element() { // Извлекает верхний элемент, но не удаляет
    if (!isEmpty()) {
      return queue[pointer];
    }
    else {
      throw new NoSuchElementException();
    }
  }

  @Override
  public E peek() { // Делает тоже самое что element и возращает null если очередь пустая
    if (!isEmpty()) {
      return queue[pointer];
    }
    else {
      return null;
    }
  }
}
