package task;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class ArrayQueueTest {
  @Test
  void size() {
    int firstElement = 12;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    assertEquals(1, arrayQueue.size());
  }

  @Test
  void isEmpty() {
    int firstElement = 12;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    assertFalse(arrayQueue.isEmpty());
    arrayQueue.remove();
    assertTrue(arrayQueue.isEmpty());
  }

  @Test
  void contains() {
    int firstElement = 12;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    assertTrue(arrayQueue.contains(firstElement));
    assertFalse(arrayQueue.contains(0));
  }

  @Test
  void iterator() {
    int firstElement = 12;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    Iterator<Integer> iterator = arrayQueue.iterator();
    assertTrue(iterator.hasNext());
    iterator.next();
    assertFalse(iterator.hasNext());
  }

  @Test
  void testToArray() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(2, arrayQueue.toArray().length);
    assertEquals(secondElement, arrayQueue.toArray()[1]);
  }

  @Test
  void add() {
    int firstElement = 12;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    assertEquals(1, arrayQueue.size());
    assertEquals(firstElement, arrayQueue.peek());
  }

  @Test
  void remove() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(secondElement, arrayQueue.remove());
    assertEquals(1, arrayQueue.size());
  }

  // @Test
  // void addAll() {
  //   List<Integer> list = new ArrayList<>();
  //   int firstElement = 12;
  //   int secondElement = 29;
  //   list.add(firstElement);
  //   list.add(secondElement);
  //   ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
  //   arrayQueue.addAll(list);
  //   assertEquals(2, arrayQueue.size());
  // }

  @Test
  void clear() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    arrayQueue.clear();
    assertEquals(0, arrayQueue.size());
  }

  @Test
  void retainAll() {
    List<Integer> list = new ArrayList<>();
    int firstElement = 12;
    int secondElement = 29;
    list.add(firstElement);
    list.add(secondElement);
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    arrayQueue.retainAll(list);
    assertEquals(2, arrayQueue.size());
  }

  @Test
  void removeAll() {
    List<Integer> list = new ArrayList<>();
    int firstElement = 12;
    int secondElement = 29;
    list.add(firstElement);
    list.add(secondElement);
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    arrayQueue.removeAll(list);
    assertEquals(0, arrayQueue.size());
  }

  @Test
  void containsAll() {
    List<Integer> list = new ArrayList<>();
    int firstElement = 12;
    int secondElement = 29;
    list.add(firstElement);
    list.add(secondElement);
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertTrue(arrayQueue.containsAll(list));
    arrayQueue.poll();
    assertFalse(arrayQueue.containsAll(list));
  }

  @Test
  void offer() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(true, arrayQueue.offer(firstElement));
    assertEquals(3, arrayQueue.size());
    assertEquals(true, arrayQueue.offer(0));
    assertEquals(4, arrayQueue.size());
  }

  @Test
  void testRemove() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(secondElement, arrayQueue.remove());
    assertEquals(1, arrayQueue.size());
    assertEquals(firstElement, arrayQueue.peek());
  }

  @Test
  void poll() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(secondElement, arrayQueue.poll());
    assertEquals(1, arrayQueue.size());
  }

  @Test
  void element() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(secondElement, arrayQueue.element());
    assertEquals(2, arrayQueue.size());
  }

  @Test
  void peek() {
    int firstElement = 12;
    int secondElement = 29;
    ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
    arrayQueue.add(firstElement);
    arrayQueue.add(secondElement);
    assertEquals(secondElement, arrayQueue.peek());
    assertEquals(2, arrayQueue.size());
  }
}
