package task;

public class OperationsOnMatrices {
    private int[][] matrix1;
    private int[][] matrix2;
    public OperationsOnMatrices(int[][] matrix1, int[][] matrix2) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }
    public int[][] multiplyMatrix() {
        if ((matrix1.length != 0 && matrix2.length != 0) && (matrix1[0].length != 0 && matrix2[0].length != 0) && (matrix1[0].length == matrix2.length)) {
            int[][] answer = new int[matrix1.length][matrix2[0].length];
            for (int i = 0; i < matrix1.length; i++) {
                for (int j = 0; j < matrix2[0].length; j++) {
                    for (int q = 0; q < matrix1[0].length; q++) {
                        answer[i][j] += matrix1[i][q] * matrix2[q][j];
                    }
                }
            }
            return answer;
        } else {
            return null;
        }
    }
    public int[][] sumMatrix() {
        if ((matrix1.length != 0 && matrix2.length != 0) && (matrix1[0].length != 0 && matrix2[0].length != 0) && (matrix1.length == matrix2.length && matrix1[0].length == matrix2[0].length)) {
            int[][] answer = new int[matrix1.length][matrix1[0].length];
            for (int i = 0; i < matrix1.length; i++) {
                for (int j = 0; j < matrix1[0].length; j++) {
                    answer[i][j] = matrix1[i][j] + matrix2[i][j];
                }
            }
            return answer;
        } else {
            return null;
        }
    }
    public  int[][] deductionMatrix() {
        if ((matrix1.length != 0 && matrix2.length != 0) && (matrix1[0].length != 0 && matrix2[0].length != 0) && (matrix1.length == matrix2.length && matrix1[0].length == matrix2[0].length)) {
            int[][] answer = new int[matrix1.length][matrix1[0].length];
            for (int i = 0; i < matrix1.length; i++) {
                for (int j = 0; j < matrix2[0].length; j++) {
                    answer[i][j] += matrix1[i][j] - matrix2[i][j];
                }
            }
            return answer;
        } else {
            return null;
        }
    }
}


