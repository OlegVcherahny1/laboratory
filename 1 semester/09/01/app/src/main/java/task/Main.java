package task;

public class Main {
    public static void main(String[] args) {
        int[][] matrix1_multiply = {{4, 5}, {1, 7}, {8, 6}};
        int[][] matrix2_multiply = {{2, 3, 5}, {6, 3, 7}};
        OperationsOnMatrices operations_multiply = new OperationsOnMatrices(matrix1_multiply, matrix2_multiply);
        int[][] answer_multiply = operations_multiply.multiplyMatrix();
        for (int[] ints : answer_multiply) {
            for (int j = 0; j < answer_multiply[0].length; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        int[][] matrix1_sum = {{4, 5}, {1, 7}, {8, 6}};
        int[][] matrix2_sum = {{2, 3}, {5, 6}, {3, 7}};
        OperationsOnMatrices operations_sum = new OperationsOnMatrices(matrix1_sum, matrix2_sum);
        int[][] answer_sum = operations_sum.sumMatrix();
        for (int[] ints : answer_sum) {
            for (int j = 0; j < answer_sum[0].length; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        int[][] matrix1_deduction = {{4, 5}, {1, 7}, {8, 6}};
        int[][] matrix2_deduction = {{2, 3}, {5, 6}, {3, 7}};
        OperationsOnMatrices operations_deduction = new OperationsOnMatrices(matrix1_deduction, matrix2_deduction);
        int[][] answer_deduction = operations_deduction.deductionMatrix();
        for (int[] ints : answer_deduction) {
            for (int j = 0; j < answer_deduction[0].length; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
