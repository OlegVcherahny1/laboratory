package task;

import task.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FigureTest {
    Triangle triangle = new Triangle();
    Polygon polygon = new Polygon();

    @Test void triangleTest1() {
        int[] array = {1, 1, 1, 1};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest2() {
        int[] array = {1, 1};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest3() {
        int[] array = {0, 0, 0, 0};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest4() {
        int[] array = {0, 0};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest5() {
        int[] array = {0, 0, 0};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest6() {
        int[] array = {0, 0, 10};
        assertFalse(triangle.triangle(array));
    }

    @Test void triangleTest7() {
        int[] array = {1, 1, 1};
        assertTrue(triangle.triangle(array));
    }
    //==================================================================================================================
    @Test void polygonTest1() {
        int[] array = {1, 1};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest2() {
        int[] array = {0, 0, 0, 0};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest3() {
        int[] array = {0, 0};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest4() {
        int[] array = {0, 0, 0};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest5() {
        int[] array = {0, 0, 10};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest6() {
        int[] array = {0, 0, 0, 10};
        assertFalse(polygon.polygon(array));
    }

    @Test void polygonTest7() {
        int[] array = {1, 1, 1};
        assertTrue(polygon.polygon(array));
    }

    @Test void polygonTest8() {
        int[] array = {1, 1, 1, 1};
        assertTrue(polygon.polygon(array));
    }
}
