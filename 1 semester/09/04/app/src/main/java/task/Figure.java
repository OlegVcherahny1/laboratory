package task;

public abstract class Figure {
    private int perimeter = 0;
    private int[] sides;
    public int getPerimeter() {
        return perimeter;
    }
    public void setSides(int[] sides) {
        this.sides = sides;
        perimeter();
    }
    public void perimeter() {
        for (int side : sides) {
            perimeter += side;
        }
    }
    public boolean checkPerimeter() {
        for (int side : sides) {
            if (side * 2 >= getPerimeter()) {
                return false;
            }
        }
        return true;
    }
}
