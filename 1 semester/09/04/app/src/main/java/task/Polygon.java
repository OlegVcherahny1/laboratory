package task;

public class Polygon extends Figure {
    public boolean polygon(int[] sides) {
        if (sides.length >= 3) {
            setSides(sides);
            return checkPerimeter();
        } else {
            return false;
        }
    }
}
