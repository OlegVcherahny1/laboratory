package task;

public class Main {
    public static void main(String[] args) {
        Triangle triangle_1 = new Triangle();
        int[] triangle_sides_1 = {1, 1, 1};

        Polygon polygon_1 = new Polygon();
        int[] polygon_sides_1 = {1, 1, 1, 1};

        Text text = new Text();
        text.printText("ok");
        text.printText(triangle_1.triangle(triangle_sides_1));
        text.printText(polygon_1.polygon(polygon_sides_1));
    }
}
