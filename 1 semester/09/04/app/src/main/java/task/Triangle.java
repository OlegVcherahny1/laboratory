package task;

public class Triangle extends Figure {
    public boolean triangle(int[] sides) {
        if (sides.length == 3) {
            setSides(sides);
            return checkPerimeter();
        } else {
            return false;
        }
    }
}
