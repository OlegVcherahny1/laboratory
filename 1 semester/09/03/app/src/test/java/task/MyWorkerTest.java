package task;

import task.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyWorkerTest {
    @Test void accountantTest(){
        MyWorker accountant = new Accountant("Лариса", "Бухгалтер", 45000);
        assertEquals("Лариса", accountant.getName());
        assertEquals("Бухгалтер", accountant.getPost());
        assertEquals(45000, accountant.getSalary());
        assertEquals("Работаю с финансами", ((Accountant) accountant).work());
    }
    //======================================================================================================================
    @Test void chiefAccountantTest(){
        MyWorker chiefAccountant = new ChiefAccountant("Глеб", "Главный бухгалтер", 50000);
        assertEquals("Глеб", chiefAccountant.getName());
        assertEquals("Главный бухгалтер", chiefAccountant.getPost());
        assertEquals(50000, chiefAccountant.getSalary());
        assertEquals("Работаю с большими финансами", ((ChiefAccountant)chiefAccountant).work());
    }
    //======================================================================================================================
    @Test void engineerTest(){
        MyWorker engineer = new Engineer("Иван", "Инженер", 35000);
        assertEquals("Иван", engineer.getName());
        assertEquals("Инженер", engineer.getPost());
        assertEquals(35000, engineer.getSalary());
        assertEquals("Я разрабатываю и оптимизирую существующие инженерные решения", ((Engineer)engineer).work());
    }
    //======================================================================================================================
    @Test void workerTest(){
        MyWorker worker = new Worker("Иван", "Рабочий", 25000);
        assertEquals("Иван", worker.getName());
        assertEquals("Рабочий", worker.getPost());
        assertEquals(25000, worker.getSalary());
        assertEquals("Простая работа", ((Worker)worker).work());
    }
}
