package task;

public class Accountant extends MyWorker{
    public Accountant(String name, String post, int salary) {
        this.name = name;
        this.post = post;
        this.salary = salary;
    }
    @Override
    public String work() {
        return "Работаю с финансами";
    }

    @Override
    public String toString() {
        return "Бугалтер [Имя - \"" + name + "\"" + ", Должность - \"" + post + "\"" + ", Зарплата - " + salary + ']';
    }
}
