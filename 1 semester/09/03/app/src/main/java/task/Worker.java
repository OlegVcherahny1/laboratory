package task;

public class Worker extends MyWorker{
    public Worker(String name, String post, int salary) {
        this.name = name;
        this.post = post;
        this.salary = salary;
    }
    @Override
    public String work() {
        return "Простая работа";
    }

    @Override
    public String toString() {
        return "Рабочий [Имя - \"" + name + "\"" + ", Должность - \"" + post + "\"" + ", Зарплата - " + salary + ']';
    }
}
