package task;

public class Engineer extends MyWorker{
    public Engineer(String name, String post, int salary) {
        this.name = name;
        this.post = post;
        this.salary = salary;
    }
    @Override
    public String work() {
        return "Я разрабатываю и оптимизирую существующие инженерные решения";
    }

    @Override
    public String toString() {
        return "Инжинер [Имя - \"" + name + "\"" + ", Должность - \"" + post + "\"" + ", Зарплата - " + salary + ']';
    }
}
