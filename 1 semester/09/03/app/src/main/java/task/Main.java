package task;

public class Main {
    public static void main(String[] args) {
        MyWorker[] myWorkers = new MyWorker[4];
        myWorkers[0] = new Accountant("Лариса", "Бухгалтер", 45000);
        myWorkers[1] = new ChiefAccountant("Глеб", "Главный бухгалтер", 50000);
        myWorkers[2] = new Engineer("Иван", "Инженер", 35000);
        myWorkers[3] = new Worker("Иван", "Рабочий", 25000);

        System.out.println("==================================================================================");
        for (MyWorker myWorker : myWorkers) {
            System.out.println(myWorker);
            System.out.println(myWorker.work());
            System.out.println("==================================================================================");
        }
    }
}
