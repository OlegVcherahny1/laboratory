package task;

public class ChiefAccountant extends MyWorker{
    public ChiefAccountant(String name, String post, int salary) {
        this.name = name;
        this.post = post;
        this.salary = salary;
    }
    @Override
    public String work() {
        return "Работаю с большими финансами";
    }

    @Override
    public String toString() {
        return "Главный бугалтер [Имя - \"" + name + "\"" + ", Должность - \"" + post + "\"" + ", Зарплата - " + salary + ']';
    }
}
