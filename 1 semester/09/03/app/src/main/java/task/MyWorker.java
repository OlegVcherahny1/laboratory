package task;

public abstract class MyWorker {
    protected String name;
    protected String post;
    protected int salary;
    protected int hash;
    public String getName() {
        return name;
    }
    public String getPost() {
        return post;
    }
    public int getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyWorker) {
            if (this.name == ((MyWorker) obj).getName() &&
                this.post == ((MyWorker) obj).getPost() &&
                this.salary == ((MyWorker) obj).getSalary()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        hash = name.length() * 100 + post.length() * 100 + salary;
        return hash;
    }
    protected abstract String work();
}
