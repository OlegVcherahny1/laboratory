package task;

public class Vector {
    private int x;
    private int y;
    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return "Вектор: (" + this.x + ";" + this.y + ")";
    }
    public Vector sumVectors(Vector other) {
        return new Vector(this.x + other.x, this.y + other.y);
    }
    public Vector subtractingVectors(Vector other) {
        return new Vector(this.x - other.x, this.y - other.y);
    }
    public Vector multiplicationScalar(int scalar) {
        return new Vector(this.x * scalar, this.y * scalar);
    }
    public Vector divisionScalar(int scalar) {
        return new Vector(this.x / scalar, this.y / scalar);
    }
}
