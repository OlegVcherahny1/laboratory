package task;

public class Main {
    public static void main(String[] args) {
        Vector vector1 = new Vector(2, 2);
        Vector vector2 = new Vector(4, 4);
        int scalar = 4;
        System.out.println("Примечание, все вектора изходит из точки (0;0)!");
        System.out.println("Суммы vector1 и vector2 рвна vector3" + vector1.sumVectors(vector2));
        System.out.println("Разность vector1 и vector2 рвна vector3" + vector1.subtractingVectors(vector2));
        System.out.println("Скалярное произведение vector1 рвно" + vector1.multiplicationScalar(scalar));
        System.out.println("Скалярное произведение vector2 рвно" + vector2.multiplicationScalar(scalar));
        System.out.println("Скалярное деление vector1 рвно" + vector1.divisionScalar(scalar));
        System.out.println("Скалярное деление vector2 рвно" + vector2.divisionScalar(scalar));
    }
}
