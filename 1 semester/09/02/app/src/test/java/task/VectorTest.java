package task;

import task.Vector;
import org.junit.jupiter.api.Test;

public class VectorTest {
    private void assertArrayEquals(Vector vector, Vector sumVectors) {}

    @Test void sumVectorsTest() {
        Vector vector1 = new Vector(2, 2);
        Vector vector2 = new Vector(4, 4);
        assertArrayEquals(new Vector(6, 6), vector1.sumVectors(vector2));
    }

    @Test void subtractingVectorsTest() {
        Vector vector1 = new Vector(2, 2);
        Vector vector2 = new Vector(4, 4);
        assertArrayEquals(new Vector(-2, -2), vector1.subtractingVectors(vector2));
    }

    @Test void multiplicationScalarTest() {
        Vector vector1 = new Vector(2, 2);
        int scalar = 4;
        assertArrayEquals(new Vector(8, 8), vector1.multiplicationScalar(scalar));
    }

    @Test void multiplicationScalarTest2() {
        Vector vector2 = new Vector(4, 4);
        int scalar = 4;
        assertArrayEquals(new Vector(16, 16), vector2.multiplicationScalar(scalar));
    }

    @Test void divisionScalarTest() {
        Vector vector1 = new Vector(2, 2);
        int scalar = 4;
        assertArrayEquals(new Vector(0, 0), vector1.divisionScalar(scalar));
    }

    @Test void divisionScalarTest2() {
        Vector vector2 = new Vector(4, 4);
        int scalar = 4;
        assertArrayEquals(new Vector(1, 1), vector2.divisionScalar(scalar));
    }
}
